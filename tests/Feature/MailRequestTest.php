<?php

namespace Tests\Feature;

use App\Mail\SendStockPriceEmail;
use Carbon\Carbon;
use Tests\TestCase;

class MailRequestTest extends TestCase
{
  /**
   * With valid params
   */
  public function test_success_validation_rules(): void
  {
    $this
      ->post("sendEmail", [
        "companyName" => "Test Company Name",
        "email" => "stojkov@protonmail.ch",
        "start" => Carbon::now()->subDays(30),
        "end" => Carbon::now()->subDays(10)
      ])->assertOk();
  }
  /**
   * Testing validation rules for `sendEmail` route
   * @test
   * @dataProvider invalidValidationRules
   */
  public function test_break_validation_rules(array $invalidData, array $invalidFields): void
  {
    $this
      ->post("sendEmail", $invalidData)
      ->assertSessionHasErrors($invalidFields)
      ->assertStatus(302);
  }

  public static function invalidValidationRules()
  {
    return [
      // Company Name not present
      [
        [
          "email" => "stojkov@protonmail.ch",
          "start" => Carbon::now()->subDays(30),
          "end" => Carbon::now()->subDays(10)
        ],
        ['companyName']
      ],
      [
        // Email not present
        [
          "companyName" => "Test Company Name",
          "start" => Carbon::now()->subDays(30),
          "end" => Carbon::now()->subDays(10)
        ],
        ['email']
      ],
      [
        // Start Date not present
        [
          "companyName" => "Test Company Name",
          "email" => "stojkov@protonmail.ch",
          "end" => Carbon::now()->subDays(10)
        ],
        ['start']
      ],
      [
        // End Date not present
        [
          "companyName" => "Test Company Name",
          "email" => "stojkov@protonmail.ch",
          "start" => Carbon::now()->subDays(30),
        ],
        ['end']
      ],
      [
        // Is Start Date after today
        [
          "companyName" => "Test Company Name",
          "email" => "stojkov@protonmail.ch",
          "start" => Carbon::now()->addDays(5),
          "end" => Carbon::now()->subDays(10)
        ],
        ['start']
      ],
      [
        // Is End Date after today
        [
          "companyName" => "Test Company Name",
          "email" => "stojkov@protonmail.ch",
          "start" => Carbon::now()->subDays(30),
          "end" => Carbon::now()->addDays(10)
        ],
        ['end']
      ],
      [
        // Is End Date before Start Date
        [
          "companyName" => "Test Company Name",
          "email" => "stojkov@protonmail.ch",
          "start" => Carbon::now()->subDays(10),
          "end" => Carbon::now()->addDays(30)
        ],
        ['end']
      ],
    ];
  }
}
