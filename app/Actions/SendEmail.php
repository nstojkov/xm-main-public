<?php

namespace app\Actions;

use Illuminate\Http\Request;
use App\Mail\SendStockPriceEmail;
use Illuminate\Support\Facades\Mail;

class SendEmail
{
  public function handle(Request $request): void
  {
    Mail::to(
      users: $request->email
    )->send(
      mailable: new SendStockPriceEmail(
        body: $request
      )
    );
  }
}
