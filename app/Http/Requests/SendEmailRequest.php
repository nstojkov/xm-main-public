<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendEmailRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   */
  public function authorize(): bool
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
   */
  public function rules(): array
  {
    return [
      'companyName' => 'required',
      'email' => 'required|email:rfc,dns',
      'start' => 'required|date|before:end|before:tomorrow',
      'end' => 'required|date|after:start|before:tomorrow'
    ];
  }
}
