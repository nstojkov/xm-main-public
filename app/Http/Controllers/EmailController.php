<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendEmailRequest;
use App\Actions\SendEmail;

class EmailController extends Controller
{
  public function sendEmail(
    SendEmailRequest $sendEmailRequest,
    SendEmail $sendEmail
  ): void {
    $sendEmail->handle(
      request: $sendEmailRequest
    );
  }
}
