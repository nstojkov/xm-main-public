# XM Test Assignment
This was an assigment from XM about a PHP Developer position.

Eventho this is a PHP assignment I did some stuff in Go because that's how I'd keep and retrieve metadata. It's a 5 minute job to transfer the querying to Meilisearch to PHP if needed.

## Omitted features
Stuff that I didn't do but would usually do (Just not in a time constrained exercise):
- Split validation and other logic out of components.
- Paginate tables, longer date ranges "break" the UI.
- Use TypeScript
- Styling can use some work
- The email sending should be done via Q
- E2E tests
