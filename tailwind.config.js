/** @type {import('tailwindcss').Config} */
const colors = require("tailwindcss/colors")

export default {
  darkMode: 'class',
  content: [
    "./index.html",
    "./resources/**/*.{js,ts,jsx,tsx,vue}",
    "./node_modules/vue-tailwind-datepicker/**/*.js"
  ],
  theme: {
    extend: {
      colors: {
        "vtd-primary": colors.sky, // Light mode Datepicker color
        "vtd-secondary": colors.gray, // Dark mode Datepicker color
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ]
}

